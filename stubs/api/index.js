const { request } = require("express");
const router = require("express").Router();

const TWO_SECONDS = 1000;


router.get("/browse/new-releases", (request, response) => {
  setTimeout(()=> {
     response.send(require ("./newReleases/releases.json"));
    // response.status(400).send("")
  }, TWO_SECONDS)
});

router.get("/artists/:id", (request, response) => {
    setTimeout(()=> {
        response.send(require ("./artist/artist.json"));
        // response.status(400).send("")
    }, TWO_SECONDS)
});

router.get("/browse/featured-playlists", (request, response) => {
    setTimeout(()=> {
        response.send(require ("./featuredPlaylists/playlists.json"));
        // response.status(400).send("")
    }, TWO_SECONDS)
});

module.exports = router;