import axios from 'axios';
import {getConfigValue} from '@ijl/cli';

const baseApiUrl = getConfigValue('jslearning');
export const bhAxios = axios.create({
    baseURL: baseApiUrl,
    headers: {
        'Content-Type': 'application/json',
        "Accept": "application/json",
        "Authorization": "Bearer " + getConfigValue('jslearning_token'),
    },
});
