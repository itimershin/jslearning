import Dashboard from "../containers/dashboard"
import { describe, it, expect } from "@jest/globals";
import { mount } from "enzyme";
import React from "react";
import{ Provider } from "react-redux"
import {StaticRouter} from "react-router-dom";
import {URLs} from "../store/urls";
import store from "../store/store";
describe("Тест отрисовки компонентов", () => {
    it("Страница релизов", () => {
        const component = mount(
            <Provider store={store}>
            <StaticRouter location={URLs.root.url}>
            <Dashboard />
            </StaticRouter>
                </Provider>
        );
        expect(component).toMatchSnapshot();
    });
    it("Страница артиста", () => {
        const component = mount(
            <Provider store={store}>
                <StaticRouter location={URLs.artistPage.url}>
                    <Dashboard />
                </StaticRouter>
            </Provider>
        );
        expect(component).toMatchSnapshot();
    });
    it("Страница плейлистов", () => {
        const component = mount(
            <Provider store={store}>
                <StaticRouter location={URLs.featuredPlaylist.url}>
                    <Dashboard />
                </StaticRouter>
            </Provider>
        );
        expect(component).toMatchSnapshot();
    });
});