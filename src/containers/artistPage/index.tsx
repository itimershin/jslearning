import React, {useEffect} from "react";
import style from "./style.css";
import {Link} from "react-router-dom"
import {useSelector, useDispatch} from "react-redux";
import * as selectors from '../../store/artists/selectors'
import {getArtist} from '../../store/artists/actions'
import {URLs} from "../../store/urls"
import Sidebar from "../../componetns/sidebar/sidebar";
import Header from "../../componetns/header/header";

const ArtistPage = (props) => {
    const isLoading = useSelector(selectors.loading);
    const error = useSelector(selectors.error);
    const data = useSelector(selectors.dataItems);
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(getArtist(props.match.params.id));
    }, []);

    if (isLoading) {
        return <div><Sidebar/><Header /><div className={style.loading} >Loading...</div></div>
    }

    if (error) {
        return error;
    }
    return (
        <div className={style.content}>
            <Sidebar />
            <Header />
            <div  className={style.artist}>
            <a href={data?.external_urls.spotify}><img src={data?.images[1].url}></img></a>
            <ul className={style.artistUl}>
                <li className={style.artistLi}><span>Artist:<a href={data?.external_urls.spotify}>  {data?.name}</a></span></li>
                <li className={style.artistLi}><span>Genre: {data?.genres[0]}</span></li>
                <li className={style.artistLi}><span>Followers in spotify: {data?.followers.total}</span></li>
                <li className={style.artistLi}><span>Spotify Popularity rate: {data?.popularity}</span></li>
            </ul>
            </div>
        </div>
    );
};


export default ArtistPage;

