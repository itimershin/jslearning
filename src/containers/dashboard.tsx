import React from "react";
import {
    Switch,
    Route,
} from "react-router-dom";
import { URLs } from "../store/urls";

import ArtistPage from "../containers/artistPage/index"
import SpotifyApp from "../containers/SpotifyApp";
import FeaturedPlaylist from "../containers/featured-playlist"
function Dashboard() {

    return (
        <Switch>
            <Route exact path={URLs.root.url} component={SpotifyApp} />
            <Route path={URLs.artistPage.url} component={ArtistPage} />
            <Route path={URLs.featuredPlaylist.url} component={FeaturedPlaylist} />
            <Route>
                <h2>OOOOOOPSSSS NOT FOUND</h2>
            </Route>
        </Switch>
    )
}

export default Dashboard;
