import React, {useEffect} from "react";
import style from "./style.css";
import {useSelector, useDispatch} from "react-redux";
import * as selectors from '../../store/featured-playlist/selectors'
import {getFeaturedPlaylist} from '../../store/featured-playlist/actions'
import Sidebar from "../../componetns/sidebar/sidebar";
import Header from "../../componetns/header/header";
const FeaturedPlaylist = () => {
    const isLoading = useSelector(selectors.loading);
    const error = useSelector(selectors.error);
    const data = useSelector(selectors.dataItems);
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(getFeaturedPlaylist());
    }, []);

    if (isLoading) {
        return <div><Sidebar/><Header /><div className={style.loading} >Loading...</div></div>
    }

    if (error) {
        return error;
    }
    return (
        <div>
        <Sidebar />
    <Header />

        <div>

            <ul className={style.playlist}>
                {data?.playlists?.items?.map((item) => (
                        <li key={item.uri}>
                            <div>{item.name}</div>
                            {/*<a href={album.artists[0].external_urls.spotify}>Link to spotify</a>*/}
                            <a href={item.external_urls.spotify}><img src={item.images[0].url}></img></a>
                        </li>

                    )
                )}
            </ul>
            {/*<pre>{JSON.stringify(data, null, 4)}</pre>*/}
        </div>
        </div>
    );
};

export default FeaturedPlaylist;
