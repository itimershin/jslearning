import React, { useState } from "react";
import ReactDom from "react-dom";
import style from "./style.css";
import Header from "../../componetns/header/header";
import Sidebar from "../../componetns/sidebar/sidebar"
import Content from "../../componetns/Content/Content"

const SpotifyApp = () => {
  return (
    <div>
      <Sidebar />
      <div className={style.content}>
        <Header />
        <Content />
      </div>
    </div>
  );
};

export default SpotifyApp;
