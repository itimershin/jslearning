import { combineReducers } from "redux";
import genre from "./genres/reducer";
import newReleases from "./new-releases/reducer"
import artist from "./artists/reducer"
import featuredPlaylist from "./featured-playlist/reducer"
import { configureStore } from "@reduxjs/toolkit";

const store = configureStore({
    reducer:
        combineReducers({
            genre,
            newReleases,
            artist,
            featuredPlaylist
        }),
});
export default store;