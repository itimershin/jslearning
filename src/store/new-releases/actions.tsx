import { bhAxios } from "../../utils/axios"
export const GET_NEW_RELEASES = "new-releases/fetch";
export const GET_NEW_RELEASES_SUCCESS = "new-releases/success";
export const GET_NEW_RELEASES_ERROR = "new-releases/error";
// export const setActive = (key) => ({ type: SET_ACTIVE, payload: { key } });


export const getNewReleases = (county="RU", limit=10, offset= 0) => async dispatch => {
    dispatch({type: GET_NEW_RELEASES});
    try{
        const response = await bhAxios("/browse/new-releases",{
            method: "GET",
        });
     dispatch({type: GET_NEW_RELEASES_SUCCESS, payload: response.data.albums})
    } catch (error){
        dispatch({type: GET_NEW_RELEASES_ERROR, payload: "неизвестная ошибка"})
    }

};

