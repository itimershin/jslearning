import { GET_NEW_RELEASES_SUCCESS,GET_NEW_RELEASES_ERROR, GET_NEW_RELEASES } from "./actions";

const initState = {
  loading: false,
  data: null,
  error: null,

};

const newReleases = (store = initState, action) => {
  const { type, payload } = action;
  switch (type) {
    case GET_NEW_RELEASES:
      return {  loading: true,
        data: null,
        error: null,
      }
    case GET_NEW_RELEASES_SUCCESS:
      return {  loading: false,
        data: payload,
        error: null,
      }
    case GET_NEW_RELEASES_ERROR:
      return {  loading: false,
        data: null,
        error: payload,
      }

    default: return initState
  }
};
export default newReleases;
