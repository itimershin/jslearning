import {createSelector} from "@reduxjs/toolkit";

 const rootSelector = createSelector(
    state => state,
     (state:any) => state.newReleases);

export const loading = createSelector( rootSelector, (state) => state.loading)
export const error = createSelector( rootSelector, (state) => state.error)
export const dataItems = createSelector( rootSelector, (state) => state.data?.items)