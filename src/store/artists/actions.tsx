import axios from "axios";
import { bhAxios } from "../../utils/axios"
export const GET_ARTIST = "artist/fetch";
export const GET_ARTIST_SUCCESS = "artist/success";
export const GET_ARTIST_ERROR = "artist/error";
// export const setActive = (key) => ({ type: SET_ACTIVE, payload: { key } });


export const getArtist = (id) => async dispatch => {
    dispatch({type: GET_ARTIST});
    try{
        const response = await bhAxios("/artists/" + id,{
            method: "GET",
        });
     dispatch({type: GET_ARTIST_SUCCESS, payload: response.data})
    } catch (error){
        dispatch({type: GET_ARTIST_ERROR, payload: "неизвестная ошибка"})
    }

};

