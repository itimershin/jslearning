import { GET_ARTIST_SUCCESS,GET_ARTIST_ERROR, GET_ARTIST } from "./actions";

const initState = {
  loading: false,
  data: null,
  error: null,

};

const artist = (store = initState, action) => {
  const { type, payload } = action;
  switch (type) {
    case GET_ARTIST:
      return {  loading: true,
        data: null,
        error: null,
      }
    case GET_ARTIST_SUCCESS:
      return {  loading: false,
        data: payload,
        error: null,
      }
    case GET_ARTIST_ERROR:
      return {  loading: false,
        data: null,
        error: payload,
      }

    default: return initState
  }
};
export default artist;
