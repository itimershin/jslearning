export const SET_ACTIVE = "genres/actives";

export const setActive = (key) => ({ type: SET_ACTIVE, payload: { key } });
