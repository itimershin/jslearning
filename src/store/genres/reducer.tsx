import { SET_ACTIVE } from "./actions";

const initState = {
  active: 'rock',
};

const genre = (store = initState, action) => {
  const { type, payload } = action;
  switch (type) {
    case SET_ACTIVE: {
      return {
        ...store,
        active: payload.key,
      };
    }
    default:
      return store;
  }
};
export default genre;
