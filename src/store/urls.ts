import { getNavigations } from "@ijl/cli";

const navigations = getNavigations("jslearning");

export const baseUrl = navigations["jslearning"];

export const URLs = {
    root: {
        url: navigations["jslearning"],
    },
    artistPage: {
        url: navigations["jslearning_artist"],
        getUrl(id){
            return navigations["jslearning_artist"].replace(":id", id)
        }
    },
    featuredPlaylist: {
        url: navigations["jslearning_playlist"],
    },
};
