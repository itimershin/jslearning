import { bhAxios } from "../../utils/axios"
export const GET_FEATURED_PLAYLIST = "new-releases/fetch";
export const GET_FEATURED_PLAYLIST_SUCCESS = "new-releases/success";
export const GET_FEATURED_PLAYLIST_ERROR = "new-releases/error";


export const getFeaturedPlaylist = () => async dispatch => {
    dispatch({type: GET_FEATURED_PLAYLIST});
    try{
        const response = await bhAxios("/browse/featured-playlists",{
            method: "GET",
        });
     dispatch({type: GET_FEATURED_PLAYLIST_SUCCESS, payload: response.data})
    } catch (error){
        dispatch({type: GET_FEATURED_PLAYLIST_ERROR, payload: "неизвестная ошибка"})
    }

};

