import {createSelector} from "@reduxjs/toolkit";

 const rootSelector = createSelector(
    state => state,
     (state:any) => state.featuredPlaylist);

export const loading = createSelector( rootSelector, (state) => state.loading)
export const error = createSelector( rootSelector, (state) => state.error)
export const dataItems = createSelector( rootSelector, (state) => state.data)