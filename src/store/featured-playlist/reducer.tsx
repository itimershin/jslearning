import {GET_FEATURED_PLAYLIST_SUCCESS,GET_FEATURED_PLAYLIST_ERROR, GET_FEATURED_PLAYLIST } from "./actions";

const initState = {
  loading: false,
  data: null,
  error: null,

};

const featuredPlaylist = (store = initState, action) => {
  const { type, payload } = action;
  switch (type) {
    case GET_FEATURED_PLAYLIST:
      return {  loading: true,
        data: null,
        error: null,
      }
    case GET_FEATURED_PLAYLIST_SUCCESS:
      return {  loading: false,
        data: payload,
        error: null,
      }
    case GET_FEATURED_PLAYLIST_ERROR:
      return {  loading: false,
        data: null,
        error: payload,
      }

    default: return initState
  }
};
export default featuredPlaylist;
