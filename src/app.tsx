import  React,{ useEffect } from "react";
import "./app.css";
import { BrowserRouter } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./store/store";
import Dashboard from "./containers/dashboard";

const App = () => {
    useEffect(() => {
        document.title = "$potifyApp"
    }, [])
  return (

    <Provider store={store}>
        <BrowserRouter>
            <Dashboard/>
      </BrowserRouter>
    </Provider>
  );
};

export default App;
