import React from "react";
import {Link} from "react-router-dom"
import style from "./sidebar.css";
import Logo from "../../assets/images/logo4.png";
import { setActive } from "../../store/genres/actions";
import { connect } from "react-redux";
import { selectActive } from '../../store/genres/selectors'
import { URLs } from "../../store/urls";

const Sidebar = () => (
  <div className={style.sidebar}>
    <img className={style.logo} src={Logo} />
      <Link className={style.sidebarBtn} to={URLs.root.url} >
          NEW SPOTIFY RELEASES
       </Link><br></br>
      <Link to={URLs.featuredPlaylist.url} className={style.sidebarBtn}>
          RECOMMENDED PLAYLISTS
      </Link>
     <div className={style.about}>All materials used in this application are taken from the official spotify api. Ildar Timershin, Ufa 2021</div>
  </div>
);

const mapStateToProps = (state) => {return{
  activeKey : selectActive(state)
}};
const mapDispatchToProps = { setActive:setActive };

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);
