import React from "react";
import style from "./header.css";

const Header = () => (
  <div className={style.header}>
    <h1>Welcome to SpotifyApp</h1>
  </div>
);

export default Header;
