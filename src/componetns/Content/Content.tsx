import React, {useEffect} from "react";
import style from "./content.css";
import {Link} from "react-router-dom"
import {useSelector, useDispatch} from "react-redux";
import * as selectors from '../../store/new-releases/selectors'
import {getNewReleases} from '../../store/new-releases/actions'
import { URLs } from "../../store/urls"
const Content = () => {
    const isLoading = useSelector(selectors.loading);
    const error = useSelector(selectors.error);
    const data = useSelector(selectors.dataItems);
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(getNewReleases());
    }, []);

    if (isLoading) {
        return <div className={style.loading} >Loading...</div>
    }

    if (error) {
        return error;
    }
    return (
        <div className={style.content}>
            <ul className={style.newReleasesUl}>
                {data?.map((album) => (

                        <li key={album.uri}>
                            <a href={album.artists[0].external_urls.spotify}>Open in Spotify</a>
                            <div className={style.artist}>ARTIST : {album.artists[0].name}</div>
                            <div>ALBUM : {album.name}</div>

                            <Link to={URLs.artistPage.getUrl(album.artists[0].id)}><img src={album.images[1].url}></img></Link>
                        </li>

                    )
                )}
            </ul>
            {/*<pre>{JSON.stringify(data, null, 4)}</pre>*/}
        </div>
    );
};

export default Content;
