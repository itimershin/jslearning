module.exports = {
    clearMocks: true,
    collectCoverage: true,
    collectCoverageFrom: [
        "./src/**/*.ts?(x)",
        "!./src/index.tsx",
        "!./src/app.tsx",
    ],
    coverageDirectory: "<rootDir>/reports/coverage",
    coverageReporters: [
        [
            "html",
            {
                subdir: "html",
            },
        ],
    ],
    moduleNameMapper: {
        "\\.(css)$": "identity-obj-proxy",
        "\\.(jpg|jpeg|png|gif|eot|otf|webp|svg|ttf|woff|woff2|mp4|webm|wav|mp3|m4a|aac|oga)$":
            "<rootDir>/__mocks__/fileMock.js",
        "@ijl/cli": "<rootDir>/__mocks__/ijl-cli.js",
    },
    preset: "ts-jest",
    roots: ["<rootDir>"],
    setupFilesAfterEnv: ["./setup-test.js"],
    snapshotSerializers: ["enzyme-to-json/serializer"],
    testEnvironment: "enzyme",
    testEnvironmentOptions: {
        enzymeAdapter: "react16",
    },

    testMatch: [
        "**/src/**/__tests__/**/*.[jt]s?(x)",
        "**/src/**/?(*.)+(spec|test).[tj]s?(x)",
    ],
};
